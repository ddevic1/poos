import csv
import os
import cv2
import numpy as np

brightness = 20
contrast = 255

for i in range(0, 10):
    with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if len(row) != 0:

                    if line_count == 0:
                        
                        line_count += 1
                    else:
                        imeSlike = row[0]
                        line_count += 1
                        # Load and display the original image
                        img = cv2.imread("DataSetOstrine/" + str(i) + "/" + imeSlike, 0) 

                        if img is not None:
                            img = np.int16(img)
                            img = img * (contrast/127+1) - contrast + brightness
                            img = np.clip(img, 0, 255)
                            img = np.uint8(img)
                            path = 'DataSetOsvjetljenje/' + str(i)
                            cv2.imwrite(os.path.join(path , imeSlike), img)
                    
