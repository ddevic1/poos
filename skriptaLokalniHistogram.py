import numpy as np
import cv2
import csv

from skimage import data
from skimage.util.dtype import dtype_range
from skimage.util import img_as_ubyte
from skimage import exposure
from skimage.morphology import disk
from skimage.filters import rank

import os

for i in range(0, 10):
    with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if len(row) != 0:

                    if line_count == 0:
                        
                        line_count += 1
                    else:
                        
                        imeSlike = row[0]
                        
                        line_count += 1
                        
                        # Load and display the original image
                        

                        if os.path.isfile("DataSetGrayScale/" + str(i) + "/" + imeSlike) :

                            img = cv2.imread("DataSetGrayScale/" + str(i) + "/" + imeSlike, 0) 

                            # Global equalize
                            img_rescale = exposure.equalize_hist(img)

                            # Equalization

                            selem = disk(7)
                            img_eq = rank.equalize(img, selem=selem)

                            path = 'DataSetLokalniHistogram/' + str(i)
                            cv2.imwrite(os.path.join(path , imeSlike), img_eq)
                            
                            
                    print(line_count)