import cv2
import os
import csv
from skimage.color import rgb2gray 
import numpy as np

for i in range(0, 10):
    with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if len(row) != 0:

                    if line_count == 0:
                        
                        line_count += 1
                    else:
                        imeSlike = row[0]
                        line_count += 1
                        # Load and display the original image
                        img = cv2.imread("DataSetMaske/" + str(i) + "/" + imeSlike)

                        if img is not None:
                            
                            kernel = np.ones((3,3),np.float32)/9
                            processed_image = cv2.filter2D(img,-1,kernel)

                            median2D = cv2.medianBlur(processed_image, 3)
                            path = 'DataSetSum/' + str(i)
                            cv2.imwrite(os.path.join(path , imeSlike), (median2D))
                    print(line_count)


