import json
from pprint import pprint as pp
import csv

with open('anotacijePOOS.json') as f:
    data = json.load(f)

klasa = -1
klase = []
for j in range(10):
    klase.append([])
for i in range(300):
    if(i % 30 == 0):
        klasa = klasa + 1
    
    indeks = data[i]['content'].find('__')
    naziv = data[i]['content'][indeks + 5:]
    naziv = naziv.replace("%20", " ")
    naziv = naziv.replace("%28", "(")
    naziv = naziv.replace("%29", ")")
    xKoordinataLD = int(data[i]["annotation"][0]["imageWidth"] * data[i]["annotation"][0]["points"][0][0])
    yKoordinataLD = int(data[i]["annotation"][0]["imageHeight"] * data[i]["annotation"][0]["points"][0][1])
    LD = (xKoordinataLD, yKoordinataLD)
    xKoordinataDG = int(data[i]["annotation"][0]["imageWidth"] * data[i]["annotation"][0]["points"][3][0])
    yKoordinataDG = int(data[i]["annotation"][0]["imageHeight"] * data[i]["annotation"][0]["points"][3][1])
    DG = (xKoordinataDG, yKoordinataDG)
    LG = (xKoordinataLD, yKoordinataDG)
    DD = (xKoordinataDG, yKoordinataLD)
    klase[klasa].append([naziv,LD,LG,DD,DG])

for i in range(10):
    with open("klasa-" + str(i) + ".csv", mode="w") as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerow(["Naziv", "LD", "LG", "DD", "DG"])
        for j in range(30):
            writer.writerow(klase[i][j])