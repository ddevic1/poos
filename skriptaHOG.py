import numpy as np
import cv2
import csv
import pprint as pp
from skimage import data
from skimage.util.dtype import dtype_range
from skimage.util import img_as_ubyte
from skimage import exposure
from skimage.morphology import disk
from skimage.filters import rank

import os

winSize = (100,100)
blockSize = (10,10)
blockStride = (5,5)
cellSize = (10,10)
nbins = 9
derivAperture = 1
winSigma = -1.
histogramNormType = 0
L2HysThreshold = 0.2
gammaCorrection = 1
nlevels = 64
useSignedGradients = True

def izracunajHOG(img):
    gx = cv2.Sobel(img, cv2.CV_32F, 1, 0)
    gy = cv2.Sobel(img, cv2.CV_32F, 0, 1)
    mag, ang = cv2.cartToPolar(gx, gy)
    bins = np.int32(nbins*ang/(2*np.pi))    # quantizing binvalues in (0...16)
    bin_cells = bins[:10,:10], bins[10:,:10], bins[:10,10:], bins[10:,10:]
    mag_cells = mag[:10,:10], mag[10:,:10], mag[:10,10:], mag[10:,10:]
    hists = [np.bincount(b.ravel(), m.ravel(), nbins) for b, m in zip(bin_cells, mag_cells)]
    hist = np.hstack(hists)     # hist is a 64 bit vector
    #print(type(hist))
    return hist


def FunkcijaHOG(input):
    hog = cv2.HOGDescriptor(winSize,blockSize,blockStride,cellSize,nbins,derivAperture,winSigma,histogramNormType,L2HysThreshold,gammaCorrection,nlevels, useSignedGradients)

    lista_deskriptora = []

    for i in range(0, 10):
        with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                line_count = 0
                for row in csv_reader:
                    if len(row) != 0:

                        if line_count == 0:
                            
                            line_count += 1
                        else:
                            
                            imeSlike = row[0]
                            
                            line_count += 1
                            
                            # Load and display the original image
                            

                            if os.path.isfile(input + str(i) + "/" + imeSlike) :

                                img = cv2.imread(input + str(i) + "/" + imeSlike, 0) 

                               
                                descriptor = hog.compute(img)
                                lista_deskriptora.append(descriptor)
                                #print(descriptor, len(descriptor))
                                
                                
                        #print(line_count)
    return lista_deskriptora

FunkcijaHOG("DataSetTrainCropped/")