import cv2
import numpy as np
import csv
import os

def dajBrojeve(koordinate):
   
    zarez = koordinate.find(',')
    prvi = koordinate[1:zarez]
    drugi = koordinate[zarez+2: len(koordinate)-1]
    print(prvi, drugi)
    return (int(prvi), int(drugi))

def kreirajMaske(input, output):
    for i in range(0, 10):
        with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                line_count = 0
                for row in csv_reader:
                    if len(row) != 0:

                        if line_count == 0:
                            
                            line_count += 1
                        else:
                            print(row)
                            imeSlike = row[0]
                            (DLx, DLy) = dajBrojeve(row[1])
                            (GDx, GDy) = dajBrojeve(row[4])
                            
                            line_count += 1
                            print(imeSlike)
                            # Load and display the original image
                            img = cv2.imread(input + str(i) + "/" + imeSlike)

                            if img is not None:

                                # Create the basic black image 
                                mask = np.zeros(img.shape, dtype = "uint8")
                                #print(mask)

                                # Draw a white, filled rectangle on the mask image
                                cv2.rectangle(mask, (DLx, DLy), (GDx, GDy), (255, 255, 255), -1)

                                # Apply the mask and display the result
                                maskedImg = cv2.bitwise_and(img, mask)
                                path = output + str(i)
                                cv2.imwrite(os.path.join(path , imeSlike), maskedImg)
                                #cv2.waitKey(0)
                        print(line_count)
                        
    

    
