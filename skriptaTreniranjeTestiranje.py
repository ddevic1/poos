import os
import csv
import cv2
import numpy as np


from skriptaHOG import FunkcijaHOG
from skriptaHOG import izracunajHOG

lista_slika = []
lista_slikaTest =[]

for i in range(0, 10):
    j = 1
    with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if len(row) != 0:

                if line_count == 0:
                    
                    line_count += 1
                else:
                    
                    imeSlike = row[0]
                    
                    line_count += 1
                    
                    # Load and display the original image
                    if os.path.isfile("DataSetTrain/" + str(i) + "/" + imeSlike) :
                         
                        #Load image
                        img = cv2.imread("DataSetTrain/" + str(i) + "/" + imeSlike, 0) 
                        lista_slika.append(img)
                    
                    if os.path.isfile("DataSetTest/" + str(i) + "/" + imeSlike) :
                         
                        #Load image
                        img = cv2.imread("DataSetTest/" + str(i) + "/" + imeSlike, 0) 
                        lista_slikaTest.append(img)


C= 15
gamma=0.5 

svm = cv2.ml.SVM_create()
svm.setType(cv2.ml.SVM_C_SVC)
# Set SVM Kernel to Radial Basis Function (RBF) 
svm.setKernel(cv2.ml.SVM_RBF)
# Set parameter C
svm.setC(C)
# Set parameter Gamma
svm.setGamma(gamma)
 

hogdata =[izracunajHOG(img) for img in lista_slika]
hogdataTest = [izracunajHOG(img) for img in lista_slikaTest]

responses = []

for i in range(0, 10):
    for j in range(19):
        responses.append(i)

trainData = np.float32(hogdata)
testData = np.float32(hogdataTest)
print("train", len(trainData))
print("test", len(testData))
np.savetxt('test.out', trainData, delimiter=',') 


svm.train(trainData, cv2.ml.ROW_SAMPLE, np.array(responses))

#svm.save("digits_svm_model.yml");

testResponse = svm.predict(testData)[1].ravel()


print(testResponse)

#print(clf.predict(testData))


