import cv2
import numpy as np
import csv
import os
from PIL import Image

def dajBrojeve(koordinate):
   
    zarez = koordinate.find(',')
    prvi = koordinate[1:zarez]
    drugi = koordinate[zarez+2: len(koordinate)-1]
    print(prvi, drugi)
    return (int(prvi), int(drugi))

def kreirajCrop(input, output):
    for i in range(0, 10):
        with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                line_count = 0
                for row in csv_reader:
                    if len(row) != 0:

                        if line_count == 0:
                            
                            line_count += 1
                        else:
                        
                            imeSlike = row[0]
                            (DLx, DLy) = dajBrojeve(row[1])
                            (GDx, GDy) = dajBrojeve(row[4])
                            if DLx > GDx:
                                DLx, GDx = GDx, DLx
                            if DLy > GDy:
                                DLy, GDy = GDy, DLy
                            line_count += 1
                            print(imeSlike)
                            
                            if os.path.isfile(input + str(i) + "/" + imeSlike) :

                                img = Image.open(input + str(i) + "/" + imeSlike)
                                cropped = img.crop((DLx+1, DLy+1, GDx-1, GDy-1)) #img[DLy+1:GDy-1, DLx+1: GDx-1]
                                path = output + str(i)
                                resized = cropped.resize((100,100), Image.ANTIALIAS)
                                resized.save(os.path.join(path , imeSlike))

