import os
import cv2


img = cv2.imread('DataSetOstrine/0/number-191 (2).png')


cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
cv2.imshow("Image", img)

cv2.waitKey(10000)

labImg = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)

l, a, b = cv2.split(labImg)

clahe = cv2.createCLAHE(clipLimit = 3.0, tileGridSize = (8, 8))

cl = clahe.apply(l)

limg = cv2.merge((cl, a, b))

final = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)

cv2.namedWindow("Image Contrast", cv2.WINDOW_NORMAL)
cv2.imshow("Image Contrast", final)

cv2.waitKey(10000)
