import numpy as np
import cv2

from skimage import data
from skimage.util.dtype import dtype_range
from skimage.util import img_as_ubyte
from skimage import exposure
from skimage.morphology import disk
from skimage.filters import rank

   
# Load an example image
img = img_as_ubyte(data.moon())


img = cv2.imread("DataSetGrayScale/0/number-191 (2).png",0) 

print(type(img))

# Global equalize
img_rescale = exposure.equalize_hist(img)

# Equalization
selem = disk(10)
img_eq = rank.equalize(img, selem=selem)

cv2.namedWindow("Masked Image", cv2.WINDOW_NORMAL)
cv2.imshow("Masked Image", img)

cv2.namedWindow("Masked Image 2", cv2.WINDOW_NORMAL)
cv2.imshow("Masked Image 2", img_eq)
cv2.waitKey(10000)