import numpy as np
import cv2
import csv

from skimage import data
from skimage.util.dtype import dtype_range
from skimage.util import img_as_ubyte
from skimage import exposure
from skimage.morphology import disk
from skimage.filters import rank

import os

for i in range(0, 10):
    with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if len(row) != 0:

                    if line_count == 0:
                        
                        line_count += 1
                    else:
                        
                        imeSlike = row[0]
                        
                        line_count += 1
                        
                        # Load and display the original image
                        

                        if os.path.isfile("DataSetTrainCropped/" + str(i) + "/" + imeSlike) :

                            img = cv2.imread("DataSetTrainCropped/" + str(i) + "/" + imeSlike, 0) 

                            m = cv2.moments(img)
                            '''
                            if abs(m['mu02']) < 1e-2:
                                # no deskewing needed. 
                                return img.copy()
                            '''
                            SZ = 98
                            # Calculate skew based on central momemts. 
                            skew = m['mu11']/m['mu02']
                            # Calculate affine transform to correct skewness. 
                            M = np.float32([[1, skew, -0.5*SZ*skew], [0, 1, 0]])
                            # Apply affine transform
                            img = cv2.warpAffine(img, M, (SZ, SZ), flags=cv2.WARP_INVERSE_MAP | cv2.INTER_LINEAR, borderMode=cv2.BORDER_TRANSPARENT)
                            path = 'DataSetOtkrivljeni/' + str(i)
                            cv2.imwrite(os.path.join(path , imeSlike), img)
                            
                            
                    print(line_count)