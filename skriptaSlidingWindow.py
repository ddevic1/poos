import argparse
import time
import cv2
import csv
import os
import numpy as np

def noviHOG(img):
    winSize = (100,100)
    blockSize = (10,10)
    blockStride = (5,5)
    cellSize = (10,10)
    nbins = 9
    derivAperture = 1
    winSigma = -1.
    histogramNormType = 0
    L2HysThreshold = 0.2
    gammaCorrection = 1
    nlevels = 64
    useSignedGradients = True

    hog = cv2.HOGDescriptor(winSize,blockSize,blockStride,cellSize,nbins,derivAperture,winSigma,histogramNormType,L2HysThreshold,gammaCorrection,nlevels, useSignedGradients)
    descriptor = hog.compute(img)

    return descriptor

fs = cv2.FileStorage("digits_svm_model.yml", cv2.FILE_STORAGE_READ)

svm = cv2.ml.SVM_create()

svm = svm.load("digits_svm_model.xml")

def sliding_window(image, stepSize, windowSize):
	# slide a window across the image
	for y in range(0, image.shape[0], stepSize):
		for x in range(0, image.shape[1], stepSize):
			# yield the current window
			yield (x, y, image[y:y + windowSize[1], x:x + windowSize[0]])

def slajdaj(path):
    image = cv2.imread(path, 0)
    (winH, winW) = (100,100)
    imeSlike = path.split('/')[2]
    windows = []
    klase = []
    koordinate = []
    for (x, y, window) in sliding_window(image, stepSize=16, windowSize=(winW, winH)):
        # if the window does not meet our desired window size, ignore it
        if window.shape[0] != winH or window.shape[1] != winW:
            continue

        # THIS IS WHERE YOU WOULD PROCESS YOUR WINDOW, SUCH AS APPLYING A
        # MACHINE LEARNING CLASSIFIER TO CLASSIFY THE CONTENTS OF THE
        # WINDOW 
        
        windowDeskriptor = [noviHOG(window)]

        windowData = np.float32(windowDeskriptor)
        Response = svm.predict(windowData)[1].ravel()
        
        if(Response[0] != -1):
            windows.append( window )
            klase.append( Response[0] )
            koordinate.append( (x, y) )

        clone = image.copy()
        cv2.rectangle(clone, (x, y), (x + winW, y + winH), (124, 252, 0), 2)
        cv2.imshow("Window", clone)
        cv2.waitKey(1)
        time.sleep(0.025)

    minBijelih = 10000
    minIndex = -1
    klasa = -1
    windowCoordinate = (0, 0)
    for i in range(len(windows)):
        brBijelih = np.sum(windows[i] == 255)
        if(brBijelih < minBijelih):
            minBijelih = brBijelih
            minIndex = i
            klasa = klase[minIndex]
            windowCoordinate = koordinate[i]

    clone = image.copy()
    font = cv2.FONT_HERSHEY_SIMPLEX                     
    img = cv2.putText(clone, str(int(klasa)), (windowCoordinate[0]+45, windowCoordinate[1]-10), font, 1, (0, 0, 0), 2, cv2.LINE_AA)
    cv2.rectangle(clone, windowCoordinate, (windowCoordinate[0] + winW, windowCoordinate[1] + winH), (124, 252, 0), 2)
    cv2.imshow("Window", clone)
    cv2.waitKey(10000)
    time.sleep(0.025)
    print(imeSlike)
    newPath = "LabeledValidationImages/" + str(int(klasa))
    cv2.imwrite(os.path.join(newPath , imeSlike), clone)


for i in range(0, 10):
        j = 1
        with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if len(row) != 0:

                    if line_count == 0:
                        
                        line_count += 1
                    else:
                        
                        imeSlike = row[0]
                        
                        line_count += 1

                        if os.path.isfile("ValidacijaResized/" + str(i) + "/" + imeSlike) :
                            slajdaj("ValidacijaResized/" + str(i) + "/" + imeSlike)                        