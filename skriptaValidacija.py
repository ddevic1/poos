import os
import csv
import cv2
import numpy as np
from sklearn.metrics import confusion_matrix

from skriptaDataSetGray import kreirajGray
from skriptaCropResize import kreirajCrop
from skriptaHistogram import kreirajHistogram
from skriptaKreiranjeMaski import kreirajMaske
from skriptaMaskiranjeNeostrina import kreirajOstrine
from skriptaHOG import FunkcijaHOG


fs = cv2.FileStorage("digits_svm_model.yml", cv2.FILE_STORAGE_READ)

svm = cv2.ml.SVM_create()

svm = svm.load("digits_svm_model.xml")

def noviHOG(img):
    winSize = (100,100)
    blockSize = (10,10)
    blockStride = (5,5)
    cellSize = (10,10)
    nbins = 9
    derivAperture = 1
    winSigma = -1.
    histogramNormType = 0
    L2HysThreshold = 0.2
    gammaCorrection = 1
    nlevels = 64
    useSignedGradients = True

    hog = cv2.HOGDescriptor(winSize,blockSize,blockStride,cellSize,nbins,derivAperture,winSigma,histogramNormType,L2HysThreshold,gammaCorrection,nlevels, useSignedGradients)
    descriptor = hog.compute(img)

    return descriptor

def dajListuSlikaZaValidaciju():
    lista_slika_validacija = []
    for i in range(0, 10):
        j = 1
        with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if len(row) != 0:

                    if line_count == 0:
                        
                        line_count += 1
                    else:
                        
                        imeSlike = row[0]
                        
                        line_count += 1
                        
                        
                        # Load and display the original image
                        if os.path.isfile("Validacija/" + str(i) + "/" + imeSlike) :
                            
                            #Load image
                            img = cv2.imread("Validacija/" + str(i) + "/" + imeSlike, 0) 
                            lista_slika_validacija.append(img)
    return lista_slika_validacija

def nacrtajLabeleNaSlici(Response):
    
    kojiResponse = 0
    for i in range(0, 10):
        j = 1
        with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if len(row) != 0:

                    if line_count == 0:
                        
                        line_count += 1
                    else:
                        
                        imeSlike = row[0]
                        
                        line_count += 1
                        
                        
                        # Load and display the original image
                        if os.path.isfile("Validacija/" + str(i) + "/" + imeSlike) :
                            
                            #Load image
                            img = cv2.imread("Validacija/" + str(i) + "/" + imeSlike, 0) 
                            font = cv2.FONT_HERSHEY_SIMPLEX
                            
                            img = cv2.putText(img, str(int(Response[kojiResponse])),   (5, 100), font, 1, (0, 0, 0), 2, cv2.LINE_AA)
                            kojiResponse += 1
                            path = "LabeledImages/" + str(i)
                            cv2.imwrite(os.path.join(path , imeSlike), img)

def main(path):
    
    
    kreirajGray(path, path)
    kreirajMaske(path, path)
    kreirajOstrine(path, path)
    kreirajHistogram(path, path)
    kreirajCrop(path, path)

    listaDeskriptora = FunkcijaHOG(path)

    print("---------------------------------------------------------------------")
    

    listaSlikaValidacija = dajListuSlikaZaValidaciju()

    print(len(listaSlikaValidacija))
    
    hogdataValidacija = [noviHOG(img) for img in listaSlikaValidacija]

    print("---------------------------------------------------------------------")
    print(hogdataValidacija)
    print(len(hogdataValidacija))

    validacijaData = np.float32(hogdataValidacija)
    Response = svm.predict(validacijaData)[1].ravel()

    print(Response)

    nacrtajLabeleNaSlici(Response)


main("Validacija/")