import os
import csv
import cv2

for i in range(0, 10):
    j = 1
    with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if len(row) != 0:

                if line_count == 0:
                    
                    line_count += 1
                else:
                    
                    imeSlike = row[0]
                    
                    line_count += 1
                    
                    # Load and display the original image
                    if os.path.isfile("DataSetTrainCropped/" + str(i) + "/" + imeSlike) :
                         
                        #Load image
                        img = cv2.imread("DataSetTrainCropped/" + str(i) + "/" + imeSlike, 0) 
                        if (j<=19):
                            path = 'DataSetTrain/' + str(i)
                        else:
                            path = 'DataSetTest/' + str(i)
                        cv2.imwrite(os.path.join(path , imeSlike), img)
                        j=j+1                        
                print(line_count)