from PIL import Image
import numpy as np
import csv
import os
import cv2

def kreirajGray(input, output):
    for i in range(0, 10):
        with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                line_count = 0
                for row in csv_reader:
                    if len(row) != 0:

                        if line_count == 0:
                            
                            line_count += 1
                        else:
                            
                            imeSlike = row[0]
                            
                            line_count += 1
                            
                            # Load and display the original image
                            

                            if os.path.isfile(input + str(i) + "/" + imeSlike) :

                                # konvertuje u grayscale image sa bit-depth 1

                                img = Image.open(input + str(i) + "/" + imeSlike)

                                grayImage = img.convert('L')

                                bitwiseImage = grayImage.point(lambda x: 0 if x < 200 else 255, '1')

                                bitwiseImage.save(output + str(i) +  "/" + imeSlike)
                                
                                
                        print(line_count)



