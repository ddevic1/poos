import csv
import os
import cv2
import numpy as np
from skimage.color import rgb2gray
from skimage.color import gray2rgb

def kreirajHistogram(input, output):
    for i in range(0, 10):
        with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                line_count = 0
                for row in csv_reader:
                    if len(row) != 0:

                        if line_count == 0:
                            
                            line_count += 1
                        else:
                            imeSlike = row[0]
                            line_count += 1
                            # Load and display the original image
                            img = cv2.imread(input+ str(i) + "/" + imeSlike, 0) 

                            if img is not None:
                                equ = cv2.equalizeHist(img)
                                path = output + str(i)
                                cv2.imwrite(os.path.join(path , imeSlike), equ)
          
