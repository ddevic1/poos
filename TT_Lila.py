import os
import csv
import cv2
import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn.externals import joblib
from PIL import Image


from skriptaHOG import FunkcijaHOG
from skriptaHOG import izracunajHOG

def noviHOG(img):
    winSize = (100,100)
    blockSize = (10,10)
    blockStride = (5,5)
    cellSize = (10,10)
    nbins = 9
    derivAperture = 1
    winSigma = -1.
    histogramNormType = 0
    L2HysThreshold = 0.2
    gammaCorrection = 1
    nlevels = 64
    useSignedGradients = True

    hog = cv2.HOGDescriptor(winSize,blockSize,blockStride,cellSize,nbins,derivAperture,winSigma,histogramNormType,L2HysThreshold,gammaCorrection,nlevels, useSignedGradients)
    descriptor = hog.compute(img)

    return descriptor

lista_slika = []
lista_slikaTest =[]

for i in range(19):
    img = Image.open("DataSetTrain/Background/backgroundImage - Copy (" + str(i) +  ").png")

    grayImage = img.convert('L')

    bitwiseImage = grayImage.point(lambda x: 0 if x < 200 else 255, '1')

    bitwiseImage.save("DataSetTrain/Background/backgroundImage - Copy (" + str(i) +  ").png")
    imgBackground = cv2.imread("DataSetTrain/Background/backgroundImage - Copy (" + str(i) +  ").png", 0)
    lista_slika.append(imgBackground)

for i in range(8):
    
    imgBackground = cv2.imread("DataSetTest/Background/backgroundImage - Copy (" + str(i) +  ").png", 0)
    lista_slikaTest.append(imgBackground)

for i in range(0, 10):
    j = 1
    with open('Anotacije/klasa-' + str(i) + '.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if len(row) != 0:

                if line_count == 0:
                    
                    line_count += 1
                else:
                    
                    imeSlike = row[0]
                    
                    line_count += 1
                    
                    # Load and display the original image
                    if os.path.isfile("DataSetTrain/" + str(i) + "/" + imeSlike) :
                         
                        #Load image
                        img = cv2.imread("DataSetTrain/" + str(i) + "/" + imeSlike, 0) 
                        lista_slika.append(img)
                    
                    if os.path.isfile("DataSetTest/" + str(i) + "/" + imeSlike) :
                         
                        #Load image
                        img = cv2.imread("DataSetTest/" + str(i) + "/" + imeSlike, 0) 
                        lista_slikaTest.append(img)



lista_deskriptora = FunkcijaHOG("DataSetTrainCropped/")

C= 50
gamma= 0.005

svm = cv2.ml.SVM_create()

svm.setType(cv2.ml.SVM_C_SVC)
# Set SVM Kernel to Radial Basis Function (RBF) 
svm.setKernel(cv2.ml.SVM_RBF)
# Set parameter C
svm.setC(C)
# Set parameter Gamma
svm.setGamma(gamma)

hogdata = [noviHOG(img) for img in lista_slika]

hogdataTest = [noviHOG(img) for img in lista_slikaTest]

responses = []

for i in range(-1, 10):
    for j in range(19):
        responses.append(i)

print("RESPONSES", responses)


trainData = np.float32(hogdata)
testData = np.float32(hogdataTest)

#np.savetxt('test.out', trainData, delimiter=',') 
svm.train(trainData, cv2.ml.ROW_SAMPLE, np.array(responses))

svm.save("digits_svm_model.xml");

testResponse = svm.predict(testData)[1].ravel()

print(testResponse)



trueValues = []
for i in range(-1, 10):
    for j in range(8):
        trueValues.append(i)

print("TRUE VALUES",trueValues)


print("CONFUSION MATRIX")

confusion_matrix = confusion_matrix(trueValues, testResponse).transpose()
print(confusion_matrix)

TP = []
FP = []
FN = []
TN = []
for i in range(len(confusion_matrix)):
    sumaFP = 0
    sumaFN = 0
    for j in range(len(confusion_matrix)):
        
        if i == j:
            TP.append(confusion_matrix[i][j])
        if i != j:
            sumaFP += confusion_matrix[i][j]
            sumaFN += confusion_matrix[j][i]

    FP.append(sumaFP)
    FN.append(sumaFN)

for i in range(len(FN)):
    TN.append(88 - TP[i] - FP[i] - FN[i])
print("TP", TP)
print("FP", FP)

print("TN", TN)
print("FN", FN)

SPC = []
SEN = []
ACC = []

for i in range(len(FN)):
    SPC.append((TN[i]/(TN[i] + FP[i]))*100)
    SEN.append((TP[i]/(TP[i] + FN[i]))*100)
    ACC.append(((TP[i] + TN[i])/(TN[i] + FP[i] + TP[i] + FN[i]))*100)

print("SPECIFICITY", SPC)

print("SENSITIVITY", SEN)

print("ACCURACY", ACC)

print("TOTAL SPECIFICITY", sum(SPC)/11)

print("TOTAL SENSITIVITY", sum(SEN)/11)

print("TOTAL ACCURACY", sum(ACC)/11)


